﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Simulacro.Models;
using Simulacro.Models.Entidades;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Simulacro.Controllers
{
    public class PagoController : Controller
    {
        protected readonly SimulacroContext cnx;
        protected readonly IConfiguration configuration;

        public PagoController(SimulacroContext cnx, IConfiguration configuration)
        {
            this.cnx = cnx;
            this.configuration = configuration;
        }


        [HttpGet]
        public ActionResult Create()
        {
            ViewBag.Cuentas = cnx.Cuentas.Include(o=>o.pagos).ToList();
            return View();
        }

        [HttpPost]
        public ActionResult Create(int idCuenta, string descripcion, decimal monto)
        {
            Cuenta cuenta = cnx.Cuentas.Where(o => o.idCuenta == idCuenta)
                .Include(o=>o.pagos).FirstOrDefault();
            var z = cuenta.pagos;
                
            var pagos = cnx.Pagos.Where(o => o.idCuenta == idCuenta).ToList();
                

                decimal saldo = cuenta.saldoInicial;

                if (saldo >= monto)
                {
                    var gasto = new Pago();
                    gasto.idCuenta = idCuenta;
                    gasto.fecha = DateTime.Now;
                    gasto.descripcion = descripcion;
                    gasto.monto = monto;
                    cnx.Pagos.Add(gasto);
                    cuenta.saldoInicial=saldo-monto;
                    cnx.Update(cuenta);
                    cnx.SaveChanges();
                }
                else
                {
                    return View();
                }
            


            

            return RedirectToAction("Index","Home");
        }
    }
}
