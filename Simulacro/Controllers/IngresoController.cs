﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Simulacro.Models;
using Simulacro.Models.Entidades;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Simulacro.Controllers
{
    public class IngresoController : Controller
    {
        protected readonly SimulacroContext cnx;
        protected readonly IConfiguration configuration;

        public IngresoController(SimulacroContext cnx, IConfiguration configuration)
        {
            this.cnx = cnx;
            this.configuration = configuration;
        }


        [HttpGet]
        public ActionResult Create()
        {
            ViewBag.Cuentas = cnx.Cuentas.Include(o => o.pagos).ToList();
            return View();
        }

        [HttpPost]
        public ActionResult Create(int idCuenta, string descripcion, decimal monto)
        {
            Cuenta cuenta = cnx.Cuentas.Where(o => o.idCuenta == idCuenta)
                .Include(o => o.ingresos).FirstOrDefault();
            var z = cuenta.ingresos;


            decimal saldo = cuenta.saldoInicial;

           
                var gasto = new Ingreso();
                gasto.idCuenta = idCuenta;
                gasto.fecha = DateTime.Now;
                gasto.descripcion = descripcion;
                gasto.monto = monto;
                cnx.Ingresos.Add(gasto);
                cuenta.saldoInicial = saldo + monto;
                cnx.Update(cuenta);
                cnx.SaveChanges();
           
            return RedirectToAction("Index", "Home");
        }
    }
}
