﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Simulacro.Models;
using Simulacro.Models.Entidades;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Simulacro.Controllers
{
    public class CuentaController : Controller
    {
        protected readonly SimulacroContext cnx;
        protected readonly IConfiguration configuration;

        public CuentaController(SimulacroContext cnx,IConfiguration configuration)
        {
            this.cnx = cnx;
            this.configuration = configuration;
        }


        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(string nombre, string categoria, decimal saldoInicial)
        {
            var cuenta = new Cuenta();
            cuenta.nombre = nombre;
            cuenta.categoria = categoria;
            cuenta.saldoInicial = saldoInicial;

            cnx.Cuentas.Add(cuenta);
            cnx.SaveChanges();

            return View();
        }

        public ActionResult ListarCuentas()
        {

            ViewBag.Cuentas = cnx.Cuentas.ToList();

            return View();
        }

        public ActionResult Detalles(int idCuenta)
        {
            decimal ingresos = 0;
            decimal pagos = 0;
            var cuenta = cnx.Cuentas.Where(o => o.idCuenta == idCuenta)
                .Include(o => o.ingresos).Include(o => o.pagos).FirstOrDefault();

            ViewBag.Ingresos = cnx.Ingresos.Where(o=>o.idCuenta == idCuenta).ToList();


            ViewBag.Pagos = cnx.Pagos.Where(o => o.idCuenta == idCuenta).ToList();

            if (ViewBag.Ingresos.Count == 0)
            {
                
            }
            else
            {
                foreach (var x in ViewBag.Ingresos)
                {
                    ingresos = ingresos + x.monto;
                }
            }
            if (ViewBag.Pagos.Count == 0)
            {
                
            }
            else
            {
                foreach (var x in ViewBag.Pagos)
                {
                    pagos = pagos + x.monto;
                }
            }

            ViewBag.CantidadInicial = cuenta.saldoInicial + pagos - ingresos;
            ViewBag.CantidadDisponible = cuenta.saldoInicial;
            return View();
        }

    }
}
