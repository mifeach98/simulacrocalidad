﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Simulacro.Models.Entidades
{
    public class Cuenta
    {
        public int idCuenta { get; set; }
        public string nombre { get; set; }
        public string categoria { get; set; }
        public decimal saldoInicial { get; set; }

        public List<Pago> pagos { get; set; }
        public List<Ingreso> ingresos { get; set; }
    }
}
