﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Simulacro.Models.Entidades
{
    public class Ingreso
    {
        public int idIngreso { get; set; }
        public int idCuenta { get; set; }
        public DateTime fecha { get; set; }
        public string descripcion { get; set; }
        public decimal monto { get; set; }

        public Cuenta cuenta { get; set; }
    }
}
