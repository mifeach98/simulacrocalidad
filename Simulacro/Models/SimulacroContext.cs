﻿using Microsoft.EntityFrameworkCore;
using Simulacro.Models.Entidades;
using Simulacro.Models.Maps;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Simulacro.Models
{
    
    public class SimulacroContext:DbContext
    {
        public SimulacroContext()
        {

        }

        public SimulacroContext(DbContextOptions<SimulacroContext> options)
        : base(options)
        {
        
        }
       

        public DbSet<Cuenta> Cuentas { get; set; }
        public DbSet<Pago> Pagos { get; set; }
        public DbSet<Ingreso> Ingresos { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new CuentaMap());
            modelBuilder.ApplyConfiguration(new PagoMap());
            modelBuilder.ApplyConfiguration(new IngresoMap());
        }
    }
}
