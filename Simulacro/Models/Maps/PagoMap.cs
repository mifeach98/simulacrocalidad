﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Simulacro.Models.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Simulacro.Models.Maps
{
    public class PagoMap : IEntityTypeConfiguration<Pago>
    {
        public void Configure(EntityTypeBuilder<Pago> builder)
        {
            builder.ToTable("Pago");
            builder.HasKey(o=>o.idPago);

            builder.HasOne(o => o.cuenta).WithMany(o => o.pagos).HasForeignKey(o=>o.idCuenta);
        }
    }
}
