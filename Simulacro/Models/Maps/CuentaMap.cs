﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Simulacro.Models.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Simulacro.Models.Maps
{
    public class CuentaMap : IEntityTypeConfiguration<Cuenta>
    {
        public void Configure(EntityTypeBuilder<Cuenta> builder)
        {
            builder.ToTable("Cuenta");
            builder.HasKey(o=>o.idCuenta);

            builder.HasMany(o => o.pagos).WithOne(o => o.cuenta).HasForeignKey(o=>o.idPago);
            builder.HasMany(o => o.ingresos).WithOne(o => o.cuenta).HasForeignKey(o=>o.idIngreso);
        }
    }
}
